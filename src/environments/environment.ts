// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --configuration=development` replaces `environment.ts` with `environment.development.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  apiVersion: 1,
  backendUrl: 'http://localhost:5000',
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */

// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
