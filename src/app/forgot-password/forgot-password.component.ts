import {Component, OnInit} from '@angular/core';

import {AuthenticationService} from '../common/services/authentication.service';
import {ToastrService} from 'ngx-toastr';

import {ForgotPasswordRequest} from '../common/models/requests/forgot-password-request.model';

import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public request: ForgotPasswordRequest;

  constructor(private service: AuthenticationService, private toastr: ToastrService) {
    this.request = new ForgotPasswordRequest();
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    this.service.forgotPassword(this.request.email).subscribe(response => {
      this.toastr.success(response.message, 'Forgot password');

      this.resetForm(form);
    }, error => {
      this.toastr.error(error.error.message, 'Forgot password');

      console.error(error);
    });
  }

  resetForm(form: NgForm): void {
    form.resetForm();

    this.request = new ForgotPasswordRequest();
  }
}
