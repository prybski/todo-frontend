import {Component, OnInit} from '@angular/core';

import {Router} from '@angular/router';

import {UserService} from '../common/services/user.service';

import {AuthenticateResponse} from '../common/models/responses/authenticate-response.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public user: AuthenticateResponse;

  constructor(private service: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.service.userObservable.subscribe(value => this.user = value, error => console.error(error));
  }

  logout(): void {
    this.service.setUser(null);

    this.router.navigate(['login']);
  }
}
