export class Response<TData> {
  wasSuccessful: boolean;
  message: string;
  data: TData;
  errors: Iterable<string>;
}
