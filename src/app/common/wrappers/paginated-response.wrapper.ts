import {Response} from './response.wrapper';

export class PaginatedResponse<TData> extends Response<TData> {
  pageNumber: number;
  pageSize: number;
  totalItems: number;
  totalPages: number;
}
