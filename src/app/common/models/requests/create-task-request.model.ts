import {TaskType} from '../../enums/task-type.enum';

export class CreateTaskRequest {
  listId: number;
  title: string;
  description: string;
  type?: TaskType;
}
