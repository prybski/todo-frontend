import {TaskType} from '../../enums/task-type.enum';

export class UpdateTaskRequest {
  id: number;
  title: string;
  description: string;
  type?: TaskType;
}
