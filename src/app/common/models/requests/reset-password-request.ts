export class ResetPasswordRequest {
  email: string;
  token: string;
  newPassword: string;

  constructor() {
    this.email = '';
    this.token = '';
    this.newPassword = '';
  }
}
