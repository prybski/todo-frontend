export class RegisterRequest {
  userName: string;
  email: string;
  password: string;

  constructor() {
    this.userName = '';
    this.email = '';
    this.password = '';
  }
}
