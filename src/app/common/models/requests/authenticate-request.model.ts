export class AuthenticateRequest {
  email: string;
  password: string;

  constructor() {
    this.email = '';
    this.password = '';
  }
}
