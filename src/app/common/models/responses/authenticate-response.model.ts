export class AuthenticateResponse {
  id: string;
  userName: string;
  email: string;
  isAuthenticated: boolean;
  token: string;
  refreshTokenExpirationDate: Date;
  roles: Iterable<string>;
}
