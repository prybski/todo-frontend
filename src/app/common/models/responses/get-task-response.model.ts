import {TaskType} from '../../enums/task-type.enum';

export class GetTaskResponse {
  id: number;
  title: string;
  description: string;
  type?: TaskType;
}
