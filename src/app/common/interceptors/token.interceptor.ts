import {Injectable} from '@angular/core';

import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';

import {Router} from '@angular/router';

import {EMPTY, Observable, throwError} from 'rxjs';

import {catchError, mergeMap} from 'rxjs/operators';

import {AuthenticationService} from '../services/authentication.service';
import {UserService} from '../services/user.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService, private userService: UserService, private router: Router) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const user = this.userService.getUser();

    if (user !== null) {
      request = request.clone({setHeaders: {Authorization: `Bearer ${user.token}`}});
    }

    return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
      if (error.status !== 401) {
        return throwError(error);
      }

      return this.authenticationService.refreshToken().pipe(mergeMap(response => {
        this.userService.setUser(response.data);

        return next.handle(request.clone({setHeaders: {Authorization: `Bearer ${response.data.token}`}}));
      }), catchError(() => {
        this.userService.setUser(null);

        this.router.navigate(['login']);

        return EMPTY;
      }));
    }));
  }
}
