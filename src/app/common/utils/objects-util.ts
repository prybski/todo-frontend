export class ObjectsUtil {
  public static getKeys(object: any): Array<string> {
    return Object.keys(object).filter(key => isNaN(key as any));
  }
}
