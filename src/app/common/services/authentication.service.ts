import {Injectable} from '@angular/core';

import {environment} from '../../../environments/environment';

import {HttpClient, HttpParams} from '@angular/common/http';

import {Observable} from 'rxjs';

import {Response} from '../wrappers/response.wrapper';

import {ConfirmEmailRequest} from '../models/requests/confirm-email-request.model';
import {RegisterRequest} from '../models/requests/register-request.model';
import {ResetPasswordRequest} from '../models/requests/reset-password-request';

import {AuthenticateResponse} from '../models/responses/authenticate-response.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = `${environment.backendUrl}/api/User`;
  }

  authenticate(email: string, password: string): Observable<Response<AuthenticateResponse>> {
    const params = new HttpParams().set('Email', email).set('Password', password);

    return this.http.get<Response<AuthenticateResponse>>(`${this.baseUrl}/Authenticate`, {params, withCredentials: true});
  }

  confirmEmail(request: ConfirmEmailRequest): Observable<Response<string>> {
    return this.http.put<Response<string>>(`${this.baseUrl}/ConfirmEmail`, request);
  }

  forgotPassword(email: string): Observable<Response<string>> {
    const params = new HttpParams().set('Email', email);

    return this.http.get<Response<string>>(`${this.baseUrl}/ForgotPassword`, {params});
  }

  refreshToken(): Observable<Response<AuthenticateResponse>> {
    return this.http.post<Response<AuthenticateResponse>>(`${this.baseUrl}/RefreshToken`, null, {withCredentials: true});
  }

  register(request: RegisterRequest): Observable<Response<string>> {
    return this.http.post<Response<string>>(`${this.baseUrl}/Register`, request);
  }

  resetPassword(request: ResetPasswordRequest): Observable<Response<string>> {
    return this.http.put<Response<string>>(`${this.baseUrl}/ResetPassword`, request);
  }
}
