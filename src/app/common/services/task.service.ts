import {Injectable} from '@angular/core';

import {environment} from '../../../environments/environment';

import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Response} from '../wrappers/response.wrapper';

import {CreateTaskRequest} from '../models/requests/create-task-request.model';
import {UpdateTaskRequest} from '../models/requests/update-task-request.model';

import {GetTaskResponse} from '../models/responses/get-task-response.model';
import {PaginatedResponse} from '../wrappers/paginated-response.wrapper';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private readonly baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = `${environment.backendUrl}/api/v${environment.apiVersion}/Task`;
  }

  create(request: CreateTaskRequest): Observable<number> {
    return this.http.post<number>(`${this.baseUrl}`, request);
  }

  delete(id: number): Observable<number> {
    return this.http.delete<number>(`${this.baseUrl}/${id}`);
  }

  get(id: number): Observable<Response<GetTaskResponse>> {
    return this.http.get<Response<GetTaskResponse>>(`${this.baseUrl}/${id}`);
  }

  getAll(pageNumber: number, pageSize: number, listId?: number): Observable<PaginatedResponse<Iterable<GetTaskResponse>>> {
    let params = new HttpParams();

    if (listId !== undefined) {
      params = params.set('listId', listId.toString());
    }

    params = params.set('PageNumber', pageNumber.toString()).set('PageSize', pageSize.toString());

    return this.http.get<PaginatedResponse<Iterable<GetTaskResponse>>>(`${this.baseUrl}`, {params});
  }

  update(request: UpdateTaskRequest): Observable<number> {
    return this.http.put<number>(`${this.baseUrl}`, request);
  }
}
