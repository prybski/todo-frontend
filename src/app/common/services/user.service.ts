import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable} from 'rxjs';

import {AuthenticateResponse} from '../models/responses/authenticate-response.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public userObservable: Observable<AuthenticateResponse>;

  private userSubject: BehaviorSubject<AuthenticateResponse>;

  constructor() {
    this.userSubject = new BehaviorSubject<AuthenticateResponse>(JSON.parse(localStorage.getItem('_user')));

    this.userObservable = this.userSubject.asObservable();
  }

  getUser(): AuthenticateResponse {
    return this.userSubject.value;
  }

  setUser(user: AuthenticateResponse): void {
    if (user == null) {
      localStorage.removeItem('_user');
    } else {
      localStorage.setItem('_user', JSON.stringify(user));
    }

    this.userSubject.next(user);
  }
}
