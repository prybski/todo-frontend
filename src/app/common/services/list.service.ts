import {Injectable} from '@angular/core';

import {environment} from '../../../environments/environment';

import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Response} from '../wrappers/response.wrapper';

import {CreateListRequest} from '../models/requests/create-list-request.model';
import {UpdateListRequest} from '../models/requests/update-list-request.model';

import {GetListResponse} from '../models/responses/get-list-response.model';
import {PaginatedResponse} from '../wrappers/paginated-response.wrapper';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  private readonly baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = `${environment.backendUrl}/api/v${environment.apiVersion}/List`;
  }

  create(request: CreateListRequest): Observable<number> {
    return this.http.post<number>(`${this.baseUrl}`, request);
  }

  delete(id: number): Observable<number> {
    return this.http.delete<number>(`${this.baseUrl}/${id}`);
  }

  get(id: number): Observable<Response<GetListResponse>> {
    return this.http.get<Response<GetListResponse>>(`${this.baseUrl}/${id}`);
  }

  getAll(pageNumber: number, pageSize: number): Observable<PaginatedResponse<Iterable<GetListResponse>>> {
    const params = new HttpParams().set('PageNumber', pageNumber.toString()).set('PageSize', pageSize.toString());

    return this.http.get<PaginatedResponse<Iterable<GetListResponse>>>(`${this.baseUrl}`, {params});
  }

  update(request: UpdateListRequest): Observable<number> {
    return this.http.put<number>(`${this.baseUrl}`, request);
  }
}
