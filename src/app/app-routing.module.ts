import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';

import {ConfirmEmailComponent} from './confirm-email/confirm-email.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ListComponent} from './list/list.component';
import {ListsComponent} from './lists/lists.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {TaskComponent} from './task/task.component';

import {AuthenticationGuard} from './common/guards/authentication.guard';
import {RedirectGuard} from './common/guards/redirect.guard';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'lists'},
  {path: 'confirm-email', component: ConfirmEmailComponent},
  {path: 'forgot-password', component: ForgotPasswordComponent},
  {path: 'list/:id', component: ListComponent, canActivate: [AuthenticationGuard]},
  {path: 'lists', component: ListsComponent, canActivate: [AuthenticationGuard]},
  {path: 'login', component: LoginComponent, canActivate: [RedirectGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'reset-password', component: ResetPasswordComponent},
  {path: 'task/:id', component: TaskComponent, canActivate: [AuthenticationGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
