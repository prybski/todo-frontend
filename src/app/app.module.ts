import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AutosizeModule} from '@techiediaries/ngx-textarea-autosize';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastrModule} from 'ngx-toastr';

import {TokenInterceptor} from './common/interceptors/token.interceptor';

import {AppComponent} from './app.component';
import {ConfirmEmailComponent} from './confirm-email/confirm-email.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ListComponent} from './list/list.component';
import {ListsComponent} from './lists/lists.component';
import {ListFormComponent} from './lists/forms/list-form/list-form.component';
import {LoginComponent} from './login/login.component';
import {NavbarComponent} from './navbar/navbar.component';
import {RegisterComponent} from './register/register.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {TaskComponent} from './task/task.component';
import {TasksComponent} from './list/tasks/tasks.component';
import {TaskFormComponent} from './list/tasks/forms/task-form/task-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmEmailComponent,
    ForgotPasswordComponent,
    ListComponent,
    ListsComponent,
    ListFormComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    ResetPasswordComponent,
    TaskComponent,
    TasksComponent,
    TaskFormComponent,
  ],
  imports: [
    AppRoutingModule,
    AutosizeModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgbPaginationModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    })
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
