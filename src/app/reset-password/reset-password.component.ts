import {Component, OnInit} from '@angular/core';

import {AuthenticationService} from '../common/services/authentication.service';
import {ToastrService} from 'ngx-toastr';

import {ResetPasswordRequest} from '../common/models/requests/reset-password-request';

import {ActivatedRoute} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  public request: ResetPasswordRequest;

  constructor(private service: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute) {
    this.request = new ResetPasswordRequest();

    this.request.token = this.route.snapshot.queryParamMap.get('token');
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    this.service.resetPassword(this.request).subscribe(response => {
      this.toastr.success(response.message, 'Reset password');

      this.resetForm(form);
    }, error => {
      this.toastr.error(error.error.message, 'Reset password');

      console.error(error);
    });
  }

  resetForm(form: NgForm): void {
    form.resetForm();

    this.request = new ResetPasswordRequest();
  }
}
