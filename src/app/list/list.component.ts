import {Component, OnInit} from '@angular/core';

import {ListService} from '../common/services/list.service';
import {ToastrService} from 'ngx-toastr';

import {ActivatedRoute} from '@angular/router';

import {GetListResponse} from '../common/models/responses/get-list-response.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public id: number;

  public list: GetListResponse;

  constructor(private service: ListService, private toastr: ToastrService, private route: ActivatedRoute) {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.service.get(this.id).subscribe(response => this.list = response.data, error => {
      this.toastr.error(error.error.message, 'List');

      console.error(error);
    });
  }
}
