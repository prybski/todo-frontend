import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {NgForm} from '@angular/forms';

import {CreateTaskRequest} from '../../../../common/models/requests/create-task-request.model';
import {UpdateTaskRequest} from '../../../../common/models/requests/update-task-request.model';

import {ObjectsUtil} from 'src/app/common/utils/objects-util';

import {TaskType} from '../../../../common/enums/task-type.enum';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {
  public ObjectsUtil = ObjectsUtil;

  public TaskType = TaskType;

  @Input()
  public listId: number;

  @Input()
  public request: any;

  @Output()
  private createTaskChange: EventEmitter<any>;

  @Output()
  private updateTaskChange: EventEmitter<any>;

  constructor() {
    this.createTaskChange = new EventEmitter<any>();

    this.updateTaskChange = new EventEmitter<any>();
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    if (this.request.id === undefined) {
      const request = this.request as CreateTaskRequest;

      request.listId = this.listId;

      this.createTaskChange.emit({form, request});
    } else {
      this.updateTaskChange.emit({form, request: this.request as UpdateTaskRequest});
    }
  }
}
