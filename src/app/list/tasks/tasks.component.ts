import {Component, Input, OnInit} from '@angular/core';

import {NgForm} from '@angular/forms';

import {BehaviorSubject} from 'rxjs';

import {TaskService} from '../../common/services/task.service';
import {ToastrService} from 'ngx-toastr';

import {GetTaskResponse} from '../../common/models/responses/get-task-response.model';
import {PaginatedResponse} from '../../common/wrappers/paginated-response.wrapper';

import {TaskType} from '../../common/enums/task-type.enum';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  public TaskType = TaskType;

  public pageNumber: number;
  public pageSize: number;

  @Input()
  public listId: number;

  public requestSubject: BehaviorSubject<any>;

  public tasks: PaginatedResponse<Iterable<GetTaskResponse>>;

  constructor(private service: TaskService, private toastr: ToastrService) {
    this.pageNumber = 1;
    this.pageSize = 5;

    this.requestSubject = new BehaviorSubject<any>({});
  }

  ngOnInit(): void {
    this.refreshList();
  }

  onPageChange(): void {
    this.refreshList();
  }

  createTask(event: any): void {
    this.service.create(event.request).subscribe(() => {
      this.toastr.success('The task has been created successfully.', 'Tasks');

      this.refreshList();

      this.resetForm(event.form);
    }, error => {
      this.toastr.error(error.error.message, 'Tasks');

      console.error(error);
    });
  }

  deleteTask(id: number): void {
    this.service.delete(id).subscribe(() => {
      this.toastr.success('The task has been deleted successfully.', 'Tasks');

      this.refreshList();
    }, error => {
      this.toastr.error(error.error.message, 'Tasks');

      console.error(error);
    });
  }

  loadForm(task: GetTaskResponse): void {
    this.requestSubject.next({...task});
  }

  refreshList(): void {
    this.service.getAll(this.pageNumber, this.pageSize, this.listId)
      .subscribe(response => this.tasks = response, error => {
        this.toastr.error(error.error.message, 'Tasks');

        console.error(error);
      });
  }

  resetForm(form: NgForm): void {
    form.resetForm();

    this.requestSubject.next({});
  }

  updateTask(event: any): void {
    this.service.update(event.request).subscribe(() => {
      this.toastr.success('The task has been updated successfully.', 'Tasks');

      this.refreshList();

      this.resetForm(event.form);
    }, error => {
      this.toastr.error(error.error.message, 'Tasks');

      console.error(error);
    });
  }
}
