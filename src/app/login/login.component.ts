import {Component, OnInit} from '@angular/core';

import {AuthenticationService} from '../common/services/authentication.service';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../common/services/user.service';

import {Router} from '@angular/router';

import {NgForm} from '@angular/forms';

import {AuthenticateRequest} from '../common/models/requests/authenticate-request.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public request: AuthenticateRequest;

  constructor(private authenticationService: AuthenticationService, private userService: UserService,
              private toastr: ToastrService, private router: Router) {
    this.request = new AuthenticateRequest();
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    this.authenticationService.authenticate(this.request.email, this.request.password).subscribe(response => {
      this.toastr.success(response.message, 'Login');

      this.userService.setUser(response.data);

      this.resetForm(form);

      this.router.navigate(['lists']);
    }, error => {
      this.toastr.error(error.error.message, 'Login');

      console.error(error);
    });
  }

  resetForm(form: NgForm): void {
    form.resetForm();

    this.request = new AuthenticateRequest();
  }
}
