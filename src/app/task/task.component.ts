import {Component, OnInit} from '@angular/core';

import {TaskService} from '../common/services/task.service';
import {ToastrService} from 'ngx-toastr';

import {ActivatedRoute} from '@angular/router';

import {GetTaskResponse} from '../common/models/responses/get-task-response.model';

import {ObjectsUtil} from '../common/utils/objects-util';

import {TaskType} from '../common/enums/task-type.enum';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  public ObjectsUtil = ObjectsUtil;

  public TaskType = TaskType;

  public id: number;

  public task: GetTaskResponse;

  constructor(private service: TaskService, private toastr: ToastrService, private route: ActivatedRoute) {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.service.get(this.id).subscribe(response => this.task = response.data, error => {
      this.toastr.error(error.error.message, 'Task');

      console.error(error);
    });
  }
}
