import {Component, OnInit} from '@angular/core';

import {ActivatedRoute} from '@angular/router';

import {AuthenticationService} from '../common/services/authentication.service';
import {ToastrService} from 'ngx-toastr';

import {Response} from '../common/wrappers/response.wrapper';

import {finalize} from 'rxjs/operators';

import {ConfirmEmailRequest} from '../common/models/requests/confirm-email-request.model';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {
  public requestCompleted: boolean;

  private request: ConfirmEmailRequest;

  public response: Response<string>;

  constructor(private service: AuthenticationService, private toastr: ToastrService, private route: ActivatedRoute) {
    this.request = new ConfirmEmailRequest(this.route.snapshot.queryParamMap.get('userId'), this.route.snapshot.queryParamMap.get('token'));
  }

  ngOnInit(): void {
    this.service.confirmEmail(this.request).pipe(finalize(() => this.requestCompleted = true))
      .subscribe(response => {
        this.toastr.success(response.message, 'Confirm e-mail address');
      }, error => {
        this.toastr.error(error.error.message, 'Confirm e-mail address');

        console.error(error);
      });
  }
}
