import {Component, OnInit} from '@angular/core';

import {AuthenticationService} from '../common/services/authentication.service';
import {ToastrService} from 'ngx-toastr';

import {NgForm} from '@angular/forms';

import {RegisterRequest} from '../common/models/requests/register-request.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public request: RegisterRequest;

  constructor(private service: AuthenticationService, private toastr: ToastrService) {
    this.request = new RegisterRequest();
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    this.service.register(this.request).subscribe(response => {
      this.toastr.success(response.message, 'Register');

      this.resetForm(form);
    }, error => {
      this.toastr.error(error.error.message, 'Register');

      console.error(error);
    });
  }

  resetForm(form: NgForm): void {
    form.resetForm();

    this.request = new RegisterRequest();
  }
}
