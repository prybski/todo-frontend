import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';

import {NgForm} from '@angular/forms';

import {CreateListRequest} from '../../../common/models/requests/create-list-request.model';
import {UpdateListRequest} from '../../../common/models/requests/update-list-request.model';

@Component({
  selector: 'app-list-form',
  templateUrl: './list-form.component.html',
  styleUrls: ['./list-form.component.css']
})
export class ListFormComponent implements OnInit {
  @Input()
  public request: any;

  @Output()
  private createListChange: EventEmitter<any>;

  @Output()
  private updateListChange: EventEmitter<any>;

  constructor() {
    this.createListChange = new EventEmitter<any>();

    this.updateListChange = new EventEmitter<any>();
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    if (this.request.id === undefined) {
      this.createListChange.emit({form, request: this.request as CreateListRequest});
    } else {
      this.updateListChange.emit({form, request: this.request as UpdateListRequest});
    }
  }
}
