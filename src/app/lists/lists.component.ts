import {Component, OnInit} from '@angular/core';

import {BehaviorSubject} from 'rxjs';

import {ListService} from '../common/services/list.service';
import {ToastrService} from 'ngx-toastr';

import {GetListResponse} from '../common/models/responses/get-list-response.model';
import {PaginatedResponse} from '../common/wrappers/paginated-response.wrapper';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {
  public pageNumber: number;
  public pageSize: number;

  public requestSubject: BehaviorSubject<any>;

  public lists: PaginatedResponse<Iterable<GetListResponse>>;

  constructor(private service: ListService, private toastr: ToastrService) {
    this.pageNumber = 1;
    this.pageSize = 5;

    this.requestSubject = new BehaviorSubject<any>({});
  }

  ngOnInit(): void {
    this.refreshList();
  }

  onPageChange(): void {
    this.refreshList();
  }

  createList(event: any): void {
    this.service.create(event.request).subscribe(() => {
      this.toastr.success('The list has been created successfully.', 'Lists');

      this.refreshList();

      this.resetForm(event.form);
    }, error => {
      this.toastr.error(error.error.message, 'Lists');

      console.error(error);
    });
  }

  deleteList(id: number): void {
    this.service.delete(id).subscribe(() => {
      this.toastr.success('The list has been deleted successfully.', 'Lists');

      this.refreshList();
    }, error => {
      this.toastr.error(error.error.message, 'Lists');

      console.error(error);
    });
  }

  loadForm(list: GetListResponse): void {
    this.requestSubject.next({...list});
  }

  refreshList(): void {
    this.service.getAll(this.pageNumber, this.pageSize).subscribe(response => this.lists = response, error => {
      this.toastr.error(error.error.message, 'Lists');

      console.error(error);
    });
  }

  resetForm(form: NgForm): void {
    form.resetForm();

    this.requestSubject.next({});
  }

  updateList(event: any): void {
    this.service.update(event.request).subscribe(() => {
      this.toastr.success('The list has been updated successfully.', 'Lists');

      this.refreshList();

      this.resetForm(event.form);
    }, error => {
      this.toastr.error(error.error.message, 'Lists');

      console.error(error);
    });
  }
}
