pipeline {
  agent any

  environment {
    JOB_PROJECT_NAME = "${(JOB_NAME.tokenize('/') as String[])[0]}"
  }

  stages {
    stage('Test build') {
      when {
        anyOf {
          branch 'stage'
          branch 'develop'

          expression { env.TAG_NAME =~ /^(v\d+.?\d*)-uat$/ }
        }
      }

      steps {
        sh 'npm install'

        sh 'npm run build'
      }
    }

    stage('Build and push image (development)') {
      when {
        branch 'develop'
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh '''
          docker build . \
            --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-development
        '''

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-development"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Build and push image (staging)') {
      when {
        branch 'stage'
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh '''
          docker build . --build-arg ANGULAR_ENVIRONMENT=staging \
            --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-staging
        '''

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-staging"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Build and push image (uat)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-uat$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh '''
          docker build . --build-arg ANGULAR_ENVIRONMENT=uat \
            --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}
        '''

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Build and push image (production)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-production$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh '''
          docker build . --build-arg ANGULAR_ENVIRONMENT=production \
            --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}
        '''

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Deploy (development)') {
      when {
        branch 'develop'
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        sh '''
          helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace development \
            --create-namespace \
            --set backendPort=1000 \
            --set image.tag=${GIT_COMMIT}-development \
            --set service.loadBalancerIP=35.234.146.145 \
            --set service.port=1080
        '''
      }
    }

    stage('Deploy (staging)') {
      when {
        branch 'stage'
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        sh '''
          helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace staging \
            --create-namespace \
            --set backendPort=2000 \
            --set image.tag=${GIT_COMMIT}-staging \
            --set service.loadBalancerIP=35.234.146.145 \
            --set service.port=2080
        '''
      }
    }

    stage('Deploy (uat)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-uat$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        sh '''
          helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace uat --create-namespace \
            --set backendPort=3000 \
            --set image.tag=${TAG_NAME} \
            --set service.loadBalancerIP=35.234.146.145 \
            --set service.port=3080
        '''
      }
    }

    stage('Deploy (production)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-production$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        sh '''
          helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace production \
            --create-namespace \
            --set backendPort=4000 \
            --set image.tag=${TAG_NAME} \
            --set service.loadBalancerIP=35.234.146.145 \
            --set service.port=4080
        '''
      }
    }
  }

  post {
    cleanup {
      cleanWs()
    }
  }
}
