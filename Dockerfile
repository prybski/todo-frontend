FROM node:lts-alpine as build-env
WORKDIR /app

EXPOSE 80

COPY package.json package-lock.json ./
RUN npm install

ENV PATH="./node_modules/.bin:$PATH"

ARG ANGULAR_ENVIRONMENT="development"

COPY . .
RUN ng build --configuration=${ANGULAR_ENVIRONMENT}

WORKDIR /app/src

FROM nginx:stable-alpine

COPY --from=build-env /app/dist/todo-frontend /usr/share/nginx/html

WORKDIR /app

COPY nginx.conf /etc/nginx/conf.d/default.conf
